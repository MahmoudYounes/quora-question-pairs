'''
A logistic regression learning algorithm example using TensorFlow library.
This example is using the MNIST database of handwritten digits
(http://yann.lecun.com/exdb/mnist/)
Author: Aymeric Damien
Project: https://github.com/aymericdamien/TensorFlow-Examples/
'''

"""
# Parameters
learning_rate = 0.01
training_epochs = 25
batch_size = 100
display_step = 1

xtf = tf.placeholder(tf.float32, [batch_size, 5000])
ytf = tf.placeholder(tf.float32, [batch_size])

W = tf.Variable(tf.zeros([5000, 1]))
b = tf.Variable(tf.zeros([1]))

pred = tf.nn.softmax(tf.matmul(xtf, W) + b) # Softmax

cost = tf.reduce_mean(-tf.reduce_sum(ytf * tf.log(pred), reduction_indices=1))

optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

filter_summary = tf.image_summary(W)
init = tf.global_variables_initializer()
saver = tf.train.Saver([W, b])
with tf.Session() as sess:
	summary_writer = tf.train
	for epoch in range(training_epochs):
		avg_cost = 0.
		total_batch = int(xtf.shape[1] / batch_size)
		# Loop over all batches
		for i in range(total_batch):
			batch_xs = np.array(x[i * batch_size : i * batch_size + batch_size,0:5000])

			#print("length batch_xs", len(batch_xs), len(batch_xs[0]))

			batch_ys = np.array(y[i * batch_size : i * batch_size + batch_size])
			#print("length batch_ys", len(batch_ys))
			# Run optimization op (backprop) and cost op (to get loss value)
			_, c = sess.run([optimizer, cost], feed_dict={xtf: batch_xs,
			                                              ytf: batch_ys})
			# Compute average loss
			avg_cost += c / total_batch
		# Display logs per epoch step
		if (epoch + 1) % display_step == 0:
			summary_writer.add_summary(filter_summary, i)
			print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost))
		print("printing variables: ")
		print(tf.trainable_variables())

	print("Optimization Finished!")

	# Test model
	correct_prediction = tf.equal(tf.argmax(pred, 1), y[0:100])
	# Calculate accuracy
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
	print("Accuracy:", accuracy.eval({xtf: x[0:batch_size], ytf: y[0:batch_size]}) * 100 , "%")

	save_path = saver.save(sess, '/home/mahmoud/models/model.ckpt')
	print("training finished. model saved to: ", save_path)


"""
