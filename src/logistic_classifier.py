from __future__ import print_function
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.externals import joblib

import pickle
import nltk
import csv
import re
import numpy as np
import os



data_ready = 0

def prepare_data(row):
	# Function to convert a raw review to a string of words
	# The input is a single string (a raw movie review), and
	# the output is a single string (a preprocessed movie review)
	#
	# 1. Remove HTML
	review_text = BeautifulSoup(row).get_text()
	#
	# 2. Remove non-letters
	letters_only = re.sub("[^a-zA-Z]", " ", review_text)
	#
	# 3. Convert to lower case, split into individual words
	words = letters_only.lower().split()
	#
	# 4. In Python, searching a set is much faster than searching
	#   a list, so convert the stop words to a set
	stops = set(stopwords.words("english"))
	#
	# 5. Remove stop words
	meaningful_words = [w for w in words if not w in stops]
	#
	# 6. Join the words back into one string separated by space,
	# and return the result.
	return(" ".join(meaningful_words))

def read_csv_file(filename = "train.csv"):
	global data_ready
	if os.path.exists("train.fin.csv"):
		data_ready = 1
		filename = "train.fin.csv"
	data = []
	with open(filename, 'rb') as csvfile:
		csv_file_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
		for row in csv_file_reader:
			data.append(row)
	return data

def write_csv_file(filename = "train.mod.csv", fieldnames = "empty", data = "empty"):
    if data == "empty":
        raise Exception("empty data. can't write to csv.")
        return
    if fieldnames == "empty":
        fieldnames = ["id", "qid1", "qid2", "question1", "question2", "is_duplicate"]
    writer = csv.DictWriter(open(filename, 'w'), fieldnames=fieldnames)
    writer.writeheader()
    for row in data:
        writer.writerow(row)

def get_questions(data):
	q1 = []
	q2 = []
	for row in data:
		q1.append(row['question1'])
		q2.append(row['question2'])
	return q1, q2

def get_labels(data):
	labels = []
	for row in data:
		labels.append(row['is_duplicate'])
	return labels

data = read_csv_file(filename = "train.mod.csv")
x = []
y = []
if data_ready == 1:
	print("data was ready and loaded successfuly")
	for row in data:
		x.append(row['question'])
		y.append(int(row['label']))

if data_ready == 0:
	print("data was not ready. loading data and preparing it")
	q1, q2 = get_questions(data)
	y = get_labels(data)
	q3 = zip(q1, q2)
	nq3 = []
	for q in q3:
		nq3.append((q[0] + q[1]))

	x = []
	for q in nq3:
		x.append(prepare_data(q))

	del q1
	del q2
	del q3
	del nq3

	sub_data = []
	for i in range(len(x)):
		sub_data.append({'question': x[i], 'label': y[i]})

	print("saving prepared data")
	write_csv_file(filename = "train.fin.csv", fieldnames = ['question', 'label'], data = sub_data)
	del sub_data


print("constructing bag of words")
vectorizer = CountVectorizer(analyzer = "word",   \
                             tokenizer = None,    \
                             preprocessor = None, \
                             stop_words = None,   \
                             max_features = 50000)


x = vectorizer.fit_transform(x)

print("constructin model")
classifier = SGDClassifier(n_jobs=-1)

# note: we should devide the data into training and validation data
begin = 0
batch_size = 10000
end = begin + batch_size
total_length = len(y) - 100000

# sklearn logisitc implementation
print("training a logisitc regression model")

idx = 0
while end < total_length:
	subx = x[begin:end].toarray()
	suby = y[begin:end]
	classifier.partial_fit(X=subx, y=suby, classes=[0, 1])
	begin = begin + batch_size
	end = end + batch_size


print("training done. and now testing")

begin = len(y) - 100000
end = begin + batch_size
accuracy = 0

idx = 0
while begin < len(y):
	xtest = x[begin:end]
	xtest = xtest.toarray()
	z = classifier.predict(xtest)
	for i in range(len(z)):
		if z[i] == y[begin + i]:
			accuracy = accuracy + 1
	begin = begin + batch_size
	end = end + batch_size

print("model accuracy = " , accuracy / (100000.0) * 100)

path = "/media/mahmoud/01D17E7170A327C0/study files/fourth year/second term/Big Data/Project/models/"
print("saving model to ", path)

# dump the model from classifier
c = pickle.dumps(classifier)
joblib.dump(c, path)
