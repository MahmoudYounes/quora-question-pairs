import csv
import numpy
from stemming.porter2 import stem
from sklearn.feature_extraction.text import TfidfVectorizer

def write_csv_file(filename = "train.mod.csv", fieldnames = "empty", data = "empty"):
	if data == "empty":
		raise Exception("empty data. can't write to csv.")
	if fieldnames == "empty":
		fieldnames = ["id", "qid1", "qid2", "question1", "question2", "is_duplicate"]
	writer = csv.DictWriter(open(filename, 'w'), fieldnames=fieldnames) 
	writer.writeheader()
	for row in data:
		writer.writerow(row)

def read_csv_file(filename = "train.csv"):
	data = []
	with open(filename, 'rb') as csvfile:
		csv_file_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
		for row in csv_file_reader:
			data.append(row)
	return data

def stem_question(q):
	"""
	stem questions. might need to remove special chars as well
	"""
	nq = ""
	for word in q.split(" "):
		nq += (stem(word) + " ")
	return nq

def stem_data(data):
	for row in data:
		# eliminate question marks
		row['question1'] = row['question1'].replace("?", "")
		row['question2'] = row['question2'].replace("?", "")
		
		# stem
		row['question1'] = stem_question(row['question1'])
		row['question2'] = stem_question(row['question2'])
		




def get_vocabulary(questions):
	vocabulary_set = set([q for q in questions])
	return vocabulary_set, len(vocabulary_set)

def get_questions(data):
	q1 = []
	q2 = []
	for row in data:
		q1.append(row['question1'])
		q2.append(row['question2'])
	return q1, q2

def main():
	data = read_csv_file()
	stem_data(data)
	questions1, questions2 = get_questions(data)
	vocab, vocab_size = get_vocabulary((questions1 + questions2))
	total_questions = zip(questions1, questions2)
	

