import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib.pyplot as plt
import seaborn as sns
from nltk.corpus import stopwords
from nltk import word_tokenize, ngrams
from sklearn import ensemble
from sklearn.model_selection import KFold
from sklearn.metrics import log_loss
import xgboost as xgb
import os
import csv

eng_stopwords = set(stopwords.words('english'))



def read_csv_file(filename = "train.mod.csv"):
	data = []
	with open(filename, 'rb') as csvfile:
		csv_file_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
		for row in csv_file_reader:
			data.append(row)
	return data

def write_csv_file(filename = "train.mod.csv", fieldnames = "empty", data = "empty"):
    if data == "empty":
        raise Exception("empty data. can't write to csv.")
        return
    if fieldnames == "empty":
        fieldnames = ["id", "qid1", "qid2", "question1", "question2", "is_duplicate"]
    writer = csv.DictWriter(open(filename, 'w'), fieldnames=fieldnames)
    writer.writeheader()
    for row in data:
        writer.writerow(row)	

def feature_extraction(row):
	que1 = str(row['question1'])
	que2 = str(row['question2'])
	out_list = []
	# get unigram features #
	unigrams_que1 = [word for word in que1.lower().split() if word not in eng_stopwords]
	unigrams_que2 = [word for word in que2.lower().split() if word not in eng_stopwords]
	common_unigrams_len = len(set(unigrams_que1).intersection(set(unigrams_que2)))
	common_unigrams_ratio = float(common_unigrams_len) / max(len(set(unigrams_que1).union(set(unigrams_que2))),1)
	out_list.extend([float(common_unigrams_len), common_unigrams_ratio])

	# get bigram features #
	bigrams_que1 = [i for i in ngrams(unigrams_que1, 2)]
	bigrams_que2 = [i for i in ngrams(unigrams_que2, 2)]
	common_bigrams_len = len(set(bigrams_que1).intersection(set(bigrams_que2)))
	common_bigrams_ratio = float(common_bigrams_len) / max(len(set(bigrams_que1).union(set(bigrams_que2))),1)
	out_list.extend([float(common_bigrams_len), common_bigrams_ratio])

	# get trigram features #
	# trigrams_que1 = [i for i in ngrams(unigrams_que1, 3)]
	# trigrams_que2 = [i for i in ngrams(unigrams_que2, 3)]
	# common_trigrams_len = len(set(trigrams_que1).intersection(set(trigrams_que2)))
	# common_trigrams_ratio = float(common_trigrams_len) / max(len(set(trigrams_que1).union(set(trigrams_que2))),1)
	# out_list.extend([float(common_trigrams_len), common_trigrams_ratio])
	return out_list

data = read_csv_file("train.mod.csv")
test_data = read_csv_file("test.csv")

features = []
labels = []
for row in data:
	features.append(feature_extraction(row))
	labels.append(float(row['is_duplicate']))

assert len(features) == len(labels)

data_test = read_csv_file("test.csv")
test_data_tr = []
ids = []
for row in data_test:
	test_data_tr.append(feature_extraction(row))
	ids.append(row['test_id'])

"""
	features_train and labels => train .. model is None
	features_test and model => test
"""
def xgboost_logistic_classifier(features_train, features_test, labels=None, model=None, save_predictions=1, filename="pred1.csv"):
	if(features_train == None) and (features_test == None):
		print "features can't be None"
		return
	if labels == None and model == None:
		print "why are you calling me -_-!"
		return

	if labels != None:
		params = {}
		params["objective"] = "binary:logistic"
		params['eval_metric'] = 'logloss'
		params["eta"] = 0.02
		params["subsample"] = 0.7
		params["min_child_weight"] = 1
		params["colsample_bytree"] = 0.7
		params["max_depth"] = 4
		params["silent"] = 1
		params["seed"] = 7
		num_rounds = 300 
		plst = list(params.items())

		xgtrain = xgb.DMatrix(features_train, label=labels)


		model = xgb.train(plst, xgtrain, num_rounds, verbose_eval=10)


	xgtest = xgb.DMatrix(test_data_tr)
	pred_test_y = model.predict(xgtest)
	#pred_test_y[pred_test_y < 0.5] = 0
	#pred_test_y[pred_test_y >= 0.5] = 1 

	print(type(pred_test_y))
	print(len(pred_test_y))
	print(pred_test_y[0:10])

	assert len(ids) == len(pred_test_y), "ids length != predicted data length."

	final_data = []
	for i in range(len(ids)):
		final_data.append({'test_id': ids[i], 'is_duplicate': pred_test_y[i]})

	if save_predictions == 1:
		write_csv_file(filename, fieldnames=['test_id', 'is_duplicate'], data=final_data)
	return final_data

def sklearn_random_forest_classifier(features_train, features_test, labels=None, model=None, save_predictions=1, filename="pred2.csv"):
	if(features_train == None) and (features_test == None):
		print "features can't be None"
		return
	if labels == None and model == None:
		print "why are you calling me -_-!"
		return
	accuracy = -1
	if labels != None:
		model = ensemble.RandomForestClassifier(n_estimators=2, criterion='gini')
		train_data = features_train[0:300000]
		test_data  = features_train[300000:]
		train_labels = labels[0:300000]
		test_labels  = labels[300000:]
		model.fit(X=train_data, y=train_labels)

		
		testp = model.predict(test_data)
		accuracy = 0
		for i in range(len(test_labels)):
			if test_labels[i] == testp[i]:
				accuracy += 1

		accuracy = accuracy / len(test_labels) * 100
	
	pred = model.predict(test_data_tr)
	assert len(ids) == len(pred), "ids length != predicted data length."

	final_data = []
	for i in range(len(ids)):
		final_data.append({'test_id': ids[i], 'is_duplicate': pred[i]})
	if save_predictions == 1:
		write_csv_file(filename, fieldnames=['test_id', 'is_duplicate'], data=final_data)
	return final_data, accuracy

fd, acc = sklearn_random_forest_classifier(features_train=features, features_test=test_data_tr, labels=labels)
print("model accuracy is ", acc)

fd2 = xgboost_logistic_classifier(features_train=features, features_test=test_data_tr, labels=labels)

