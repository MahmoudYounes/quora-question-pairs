from __future__ import print_function
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.externals import joblib

import pickle
import nltk
import csv
import re
import numpy as np
import os
import sys



data_ready = 0
model_path = '/home/mahmoud/models/model.ckpt'

def prepare_data(row):
	# Function to convert a raw review to a string of words
	# The input is a single string (a raw movie review), and
	# the output is a single string (a preprocessed movie review)
	#
	# 1. Remove HTML
	review_text = BeautifulSoup(row).get_text()
	#
	# 2. Remove non-letters
	letters_only = re.sub("[^a-zA-Z]", " ", review_text)
	#
	# 3. Convert to lower case, split into individual words
	words = letters_only.lower().split()
	#
	# 4. In Python, searching a set is much faster than searching
	#   a list, so convert the stop words to a set
	stops = set(stopwords.words("english"))
	#
	# 5. Remove stop words
	meaningful_words = [w for w in words if not w in stops]
	#
	# 6. Join the words back into one string separated by space,
	# and return the result.
	return(" ".join(meaningful_words))

def read_csv_file(filename = "test.csv"):
	global data_ready
	if os.path.exists("test.fin.csv"):
		data_ready = 1
		filename = "test.fin.csv"
	data = []
	with open(filename, 'rb') as csvfile:
		csv_file_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
		for row in csv_file_reader:
			data.append(row)
	return data

def write_csv_file(filename = "train.mod.csv", fieldnames = "empty", data = "empty"):
    if data == "empty":
        raise Exception("empty data. can't write to csv.")
        return
    if fieldnames == "empty":
        fieldnames = ["test_id", "is_duplicate"]
    writer = csv.DictWriter(open(filename, 'w'), fieldnames=fieldnames)
    writer.writeheader()
    for row in data:
        writer.writerow(row)

def get_questions(data):
	q1 = []
	q2 = []
	for row in data:
		q1.append(row['question1'])
		q2.append(row['question2'])
	return q1, q2

def get_test_ids(data):
	test_ids = []
	for row in data:
		test_ids.append(row['test_id'])
	return test_ids


def tokenize(text):
	"""
	used by tfidf vectorizer
	"""
	tokens = nltk.word_tokenize(text)
	return tokens

data = read_csv_file(filename = "test.csv")
x = []
y = []
test_ids = []

if data_ready == 1:
	print("data was ready and loaded successfuly")
	for row in data:
		x.append(row['question'])
		test_ids.append(row['test_id'])

if data_ready == 0:
	print("data was not ready. loading data and preparing it")
	q1, q2 = get_questions(data)
	test_ids = get_test_ids(data)
	q3 = zip(q1, q2)
	nq3 = []
	for q in q3:
		nq3.append((q[0] + q[1]))

	x = []
	for q in nq3:
		x.append(prepare_data(q))

	del q1
	del q2
	del q3
	del nq3

	sub_data = []
	for i in range(len(x)):
		sub_data.append({'test_id': test_ids[i], 'question': x[i]})

	print("saving prepared data")
	write_csv_file(filename = "test.fin.csv", fieldnames = ['test_id', 'question'], data = sub_data)
	del sub_data


path = "/media/mahmoud/01D17E7170A327C0/study files/fourth year/second term/Big Data/Project/models/"
# loading model
model = joblib.load(path + 'first_model.pkl')
classifier = pickle.loads(model)

count_vec = CountVectorizer(analyzer="word", tokenizer=None, preprocessor=None, stop_words=None, max_features=50000)
tfidf_vec = TfidfVectorizer(norm='l2', min_df=0, use_idf=True, smooth_idf=False, sublinear_tf=True, tokenizer=tokenize, max_features=50000)

all_test_tfidf = tfidf_vec.fit_transform(x)
all_test_count = count_vec.fit_transform(x)
print(all_test_count.shape)
print(all_test_tfidf.shape)
print(len(test_ids))

begin = 0
batch_size = 1000
end = batch_size
while True:
	if begin >= all_test_tfidf.shape[0] and end >= all_test_tfidf.shape[0]:
		break
	subx1 = all_test_tfidf[begin:end].toarray()
	subx2 = all_test_count[begin:end].toarray()
	subx = np.column_stack((subx1, subx2))
	suby = classifier.predict_proba(X=subx)
	subids = test_ids[begin:end]
	subdata = zip(suby, subids)
	for data in subdata:
		y.append({'test_id': data[1], 'is_duplicate': (data[0][0] + data[0][1]) / 2})
	begin = begin + batch_size
	end = min(end + batch_size, len(x))

assert len(y) == len(test_ids), "lengths of predictions and input ids does not match."
print(len(y))
print(y[0:10])
# data to write should be ready as test_id is_duplicate
write_csv_file(filename="preds3.csv", fieldnames=['test_id', 'is_duplicate'], data=y)
